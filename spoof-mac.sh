
#!/bin/sh

# /etc/NetworkManager/dispatcher.d/pre-up.d/randomize-mac-addresses.sh

# Orignal Article:
# See https://www.paulfurley.com/randomize-your-wifi-mac-address-on-ubuntu-1604-xenial/


export PATH=$PATH:/usr/bin:/bin

LOG_FILE=/var/log/randomize-mac-addresses

echo "$(date): $*" > ${LOG_FILE}

WIFI_UUIDS=$(nmcli --fields type,uuid connection show |grep wifi |cut '-d ' -f3)

for UUID in ${WIFI_UUIDS}
do
    UUID_DAILY_HASH=$(echo "${UUID}-$(date +%F)" | md5sum)

    RANDOM_MAC="02:$(echo -n ${UUID_DAILY_HASH} | sed 's/^\(..\)\(..\)\(..\)\(..\)\(..\).*$/\1:\2:\3:\4:\5/')"

    CMD="nmcli connection modify ${UUID} wifi.cloned-mac-address ${RANDOM_MAC}"

    echo "$CMD" >> ${LOG_FILE}
    $CMD &
done

wait